using Feather
using FastGroupBy
using Requests
using DataStructures
using ResumableFunctions

import Requests: get

function get_timetable()#(filename)
    filename = "/Users/almacmillan/Sites/timhorton/data/schedules.feather"
    timetable = Feather.read(filename, use_mmap=true)
    timetable = convert(Array, timetable)
    return timetable

end

#=function get_stations()
    nodes = Requests.json(get("https://atomised:at0m1sed@api.fairer.com/v1/nodes/", compressed=true))["data"]
    ids_to_crs = Dict(node["id"]=>(node["codes"]["crs"], node["name"]) for node in nodes if "crs" in keys(node["codes"]))
    return stations
end=#


function find_origins(origin_idx, timetable)

    origins = timetable[timetable[:,3] .== 421, :]
    return origins

end

function konnectionscan(timetable::Array{Int64,2}, origin_idx::Int64, destination_idx::Int64)
    maxint = maximum(view(timetable, :,1:2)) + 1
    x,y = size(timetable)
    z = typeof(timetable)
    departures = find_origins(origin_idx, timetable)
    schidview = view(departures, :,5)
    uniq_departure_schids = unique(schidview)
    uniq_departure_arrs = unique(view(departures, :,4))
    unique_counts = sumby(schidview, ones(Int64, size(schidview)[1]))
    earliest_arrival = fill(maxint, (size(uniq_departure_schids)[1], 3745))
    in_connection = Dict(x=>false for x in 0:3744)
    in_connection_array = fill(maxint, (size(uniq_departure_schids)[1], 3745))        
    acc = 1
    for (i, schid) in enumerate(uniq_departure_schids)
        ix = unique_counts[schid]
        dep_slice = view(departures, acc:acc+ix-1,:)
        earliest_arrival[i,origin_idx] = dep_slice[1,:][1]
        for itx in acc:acc+ix-1
            row = view(departures, itx,:)
            arr = row[4]
            earliest_arrival[i,arr] = row[2]
            in_connection_array[i,arr] = row[6] + 1 # For Julia lookup
            in_connection[arr] = true
        end
        acc += ix
    #
    end
    println(earliest_arrival[:,origin_idx])
    @simd for i in 1:size(timetable)[1]
        noarr = false
        row = timetable[i,:]
        if in_connection[row[3]]
            if row[4] != origin_idx
                dep = view(earliest_arrival, :,row[3])
                if in_connection[row[4]] == false
                    noarr = true
                    for i in updat(row[1], row[2], dep)
                        in_connection_array[i,row[4]] = row[6] + 1
                        earliest_arrival[i,row[4]] = row[2]
                        in_connection[row[4]] = true
                    end
                else
                    #=arr = view(earliest_arrival, :,row[4])
                    for i in updat_zip(row[1], row[2], dep, arr)
                        in_connection_array[i,row[4]] = row[6] + 1
                        earliest_arrival[i,row[4]] = row[2]
                    end=#
                    arr = view(earliest_arrival, :,row[4])
                    update = (row[1] .>= dep) .& (row[2] .< arr)
                    if any(update)
                        @views in_connection_array[:,row[4]][update] = row[6] + 1
                        @views earliest_arrival[:,row[4]][update] = row[2]  
                    end
                end
            end
        end    
    end
    println(view(earliest_arrival, :,destination_idx))
    println(view(in_connection_array, :,destination_idx))
end

@resumable function updat(depT::Int64, arrT::Int64, dep) :: Int64
    for (i,x) in enumerate(dep)
        @inbounds if depT .> x
            @yield i
       end
    end
    i
end

#=@resumable function updat_zip(depT::Int64, arrT::Int64, dep, arr) :: Int64
    for (i,(x,y)) in enumerate(zip(dep,arr))
        if depT .> x & arrT .< y
            @yield i
       end
    end
    i
end=#

function main()
    timetable = get_timetable()
    konnectionscan(timetable, 421, 1553)
end


main()
